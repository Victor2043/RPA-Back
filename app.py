
from flask import Flask, jsonify
from RPA import RPA_News, RPA_Shooping


app = Flask(__name__)

@app.route('/home', methods=['GET'])

def home():
    response = jsonify(message="Simple server is running")
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

    
@app.route('/shopping/<search>', methods=['GET'])
def shopping(search):
    rpa_shopping = RPA_Shooping.RPA_Shopping(search=search)
    jsonData = rpa_shopping.getProducts()
    response = jsonify(message=jsonData)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

@app.route('/news', methods=['GET'])
def news():
    rpa_news = RPA_News.RPA_News()
    jsonData = rpa_news.getNews()
    response = jsonify(message=jsonData)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response
    

if __name__ == '__main__':
    app.run(debug=False)