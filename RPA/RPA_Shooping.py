from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time

class RPA_Shopping:
    def __init__(self, search):
        self.search = search

    def getProducts(self):
        try : 
            driver = webdriver.Chrome("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe")
        except:
            return "Diretorio nao encontrado"

        def searchProductsFirstTry(self):
                
                driver.get('https://www.google.com.br/search?q='+self.search+'&hl=pt-BR&tbm=shop&tbs=p_ord:rv')
            
                jsonData = {}
                jsonData['compras'] = []

                photos = driver.find_elements_by_xpath('//*[@class="ABtyec"]/div/img')
                
                titles = driver.find_elements_by_xpath('//*[@class="OzIAJc"]')
                if len(titles) == 0:
                   titles = driver.find_elements_by_xpath('//*[@class="xsRiS"]')
                
                sites = driver.find_elements_by_xpath('//*[@class="IHk3ob"]/a')
                if len(sites) == 0:
                    sites = driver.find_elements_by_xpath('//*[@class="b07ME mqQL1e"]/span')
                    
                links = driver.find_elements_by_xpath('//*[@class="wV5R6d"]/a')
                if len(links) == 0:
                    links = driver.find_elements_by_xpath('//*[@class="IHk3ob"]/a')
                
                prices = driver.find_elements_by_xpath('//*[@class="a8Pemb"]')
                if len(prices) == 0:
                    values = ['R$ 00,00']
                    prices = driver.find_elements_by_xpath('//*[@class="IHk3ob"]/span/span/span')
                    for i in range(2, 19, 2):
                        values.append(prices[i].text)
                        
                    for i in range(1,9):
                        price = values[i]
                        jsonData['compras'].append({
                            'title': titles[i].text,
                            'photo': photos[i].get_attribute("src"),
                            'preco': price,
                            'site' : sites[i].text,
                            'link' : str(links[i].get_attribute("href"))
                        })
                else:    
                    for i in range(1,9):
                        price = prices[i].text
                        jsonData['compras'].append({
                            'title': titles[i].text,
                            'photo': photos[i].get_attribute("src"),
                            'preco': price,
                            'site' : sites[i].text,
                            'link' : str(links[i].get_attribute("href"))
                        })
                
                return jsonData   
        
        def searchProductsSecondTry(self):
               
                driver.get('https://www.google.com.br/search?q='+self.search+'&hl=pt-BR&tbm=shop&tbs=p_ord:rv')
        
                jsonData = {}
                jsonData['compras'] = []

                photo = driver.find_elements_by_xpath('//*[@class="ArOc1c"]/img')
                if(len(photo) == 0):
                    photo = driver.find_elements_by_xpath('//*[@class="MUQY0"]/img')
                    
                titles = driver.find_elements_by_xpath('//*[@class="A2sOrd"]')

                sites = driver.find_elements_by_xpath('//*[@class="a3H7pd r29r0b shntl"]')
                if len(sites) == 0:
                    sites = driver.find_elements_by_xpath('//*[@class="aULzUe IuHnof"]')
                    
                links = driver.find_elements_by_xpath('//*[@class="eaGTj mQaFGe shntl"]')
                if(len(links) == 0):
                    links = driver.find_elements_by_xpath('//*[@class="a3H7pd r29r0b shntl"]')
                    
                prices = driver.find_elements_by_xpath('//*[@class="a8Pemb"]')    
                if len(prices) == 0:  
                    values = ['R$ 00,00']
                    prices = driver.find_elements_by_xpath('//*[@class="kHxwFf"]/span/span')
                    for i in range(2, 19, 2):
                        values.append(prices[i].text)
                    
                    for i in range(1, 9):
                       price = values[i]
                       jsonData['compras'].append({
                            'title': titles[i].text,
                            'photo': photo[i].get_attribute("src"),
                            'preco': price,
                            'site': sites[i].text,
                            'link': str(links[i].get_attribute("href"))
                        })
                else:
                    for i in range(1, 9):
                       price = prices[i].text
                       jsonData['compras'].append({
                            'title': titles[i].text,
                            'photo': photo[i].get_attribute("src"),
                            'preco': price,
                            'site': sites[i].text,
                            'link': str(links[i].get_attribute("href"))
                        })
                       
                return jsonData

        try :
            jsonData = searchProductsFirstTry(self)
            driver.close()
            return jsonData
        except :
            try : 
                jsonData = searchProductsSecondTry(self) 
                driver.close()         
                return jsonData
                
            except NoSuchElementException as e:
                return str(e)
        
    
            
    
  
        
            

        
    