from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time

class RPA_News:
    def __init__(self):
        pass
    
    def getNews(self):
        driver = webdriver.Chrome("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe")
        
        #driver.get("https://www.google.com/search?client=firefox-b-d&biw=1920&bih=461&tbm=shop&sxsrf=ALeKk02PPlug2GVaXoFXVHPhgE2eWZ3XTA%3A1615642888962&ei=CMFMYN-pOvSl5OUPjcSVoAM&q=demon%27s+souls+ps5&oq=demon%27s+souls+ps5&gs_lcp=Cgtwcm9kdWN0cy1jYxAMMgQIIxAnMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAOgoIABCxAxCDARBDOggIABCxAxCDAToHCCMQ6gIQJzoECAAQQ1CMpgNYuM8FYNyzBmgDcAB4AIABkLQBiAGB-AGSAQcwLjcuOS0zmAEAoAEBsAEKwAEB&sclient=products-cc&ved=0ahUKEwjfzevjsq3vAhX0ErkGHQ1iBTQQ4dUDCAo")
        driver.get("https://www.msn.com/pt-br/noticias?ocid=mailsignout")
        time.sleep(2)
        jsonData = {}
        jsonData['noticias'] = []

        photo = driver.find_elements_by_xpath('//*[@class="rc rcp"]/a/img')
        title = driver.find_elements_by_xpath('//*[@class="rc rcp"]/a/h3')
            
        for i in range(1, 9):
            
            jsonData['noticias'].append({
                'title': title[i].text,
                'photo': photo[i].get_attribute("src")
            })
        print(jsonData)
        driver.close()
       
        return jsonData
        